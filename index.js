
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Stanciu";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache=new Object();
   cache.pageAccessCounter=function(section){
       if(typeof(section)==='undefined'){
           section='home';
       }
       section=new String(section).toLowerCase();
       if(cache.hasOwnProperty(section)){
           cache[section]+=1;
       }
       else{
           Object.defineProperty(cache,section,{value:1,writable:true});        
       }
   };
   cache.getCache=function(){
       return cache;
   };
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

